﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Calculator : MonoBehaviour
{
    [SerializeField]
    Text inputField;
    double[] numbers=new double[2];
    bool isSmooth;
    bool isPlus;
    bool isPercent;
    double temp = 0;
    int count = 0;
    double res;
    string symbol;
    bool minusState = false;
    public void ButtonNumberPressed()
    {        
        Debug.Log(EventSystem.current.currentSelectedGameObject.name);
        string buttonValue = EventSystem.current.currentSelectedGameObject.name;
        inputField.text += buttonValue;
        isPlus = true;
    }

    public void MinusOperation()
    {       
        string buttonValue = EventSystem.current.currentSelectedGameObject.name;
        Debug.Log(buttonValue + "MinusOperation");
        if (inputField.text.Equals("-")) return;
        Debug.Log(inputField.text + "MinusOperation");
        SupportMethod(buttonValue);        
        minusState = true;
    }

    private void SupportMethod(string symbol)
    {
        
        if (double.TryParse(inputField.text, out temp))
        {
            temp = double.Parse(inputField.text);
            Debug.Log(temp + "int.TryParse-------------------------");
            numbers[count] = temp;
            count++;
        }
        else
        {
            Debug.Log("ERROR"+"SupportMethod");
        }        
        inputField.text = symbol;
        Debug.Log(symbol);
        isSmooth = true;
        this.symbol = symbol;
    }
    public void PlusOperator()
    {
        if (isPlus)
        {
            string buttonValue = EventSystem.current.currentSelectedGameObject.name;
            SupportMethod(buttonValue);
        }
        
        isPlus = false;
        
    }

    public void MultiplicationOperation()
    {
        string buttonValue = EventSystem.current.currentSelectedGameObject.name;
        SupportMethod(buttonValue);
    }

    public void Smooth()
    {
        
        if (isSmooth) 
        {
            res = 0;
            string str = inputField.text;
            Debug.Log(str);            
            double num = 0;
            num=SuportMethodTryParse(str);
            if (num == -1) return;
            if (minusState)
            {                
                minusState = false;
            }            
            numbers[count] = num;            
            SwithcMethod();    
            inputField.text = "";
            inputField.text = res.ToString();
            count = 0;
            numbers[count] = res;
            temp = 0;
        }
        isSmooth = false;
    }
    public void SwithcMethod()
    {
        for (int i = 0; i < numbers.Length; i++)
        {
            switch (symbol)
            {
                case "+":
                    Debug.Log(numbers[i] + "foreach");
                    if (inputField.text.Contains("%"))
                    {
                        if (i == 1)
                        {
                            res = numbers[i - 1]+(numbers[i - 1] * numbers[i])/100;
                        }
                    }
                    else
                    {
                        res = res + numbers[i];
                    }                    
                    break;
                case "^":
                    if (res == 0) res = 1;
                    if (i == 1)
                    {
                        res=Mathf.Pow((float)numbers[i - 1], (float)numbers[1]);
                    }
                    
                    //res = res ^ numbers[i];
                    break;
                case "/":
                    if (i == 1)
                    {
                        res = numbers[i - 1] / numbers[i];
                    }
                    break;
                case "*":
                    Debug.Log(res + "MultiplicationOperation");
                    if (res == 0) res = 1;
                    res = res * numbers[i];
                    break;

                case "-":
                    //Должен стоять +
                    if (inputField.text.Contains("%"))
                    {
                        if (i == 1)
                        {
                            res = numbers[i - 1]+(numbers[i - 1] * numbers[i]) / 100;
                        }
                    }
                    else
                    {
                        res = res + numbers[i];
                    }                    
                    break;
            }

        }
    }
    public void DivisionMethod()
    {
        string buttonValue = EventSystem.current.currentSelectedGameObject.name;
        SupportMethod(buttonValue);
    }

    public void ExponentiationMethod()
    {
        string buttonValue = EventSystem.current.currentSelectedGameObject.name;
        SupportMethod(buttonValue);
    }

    public void DeleteAllSymbol()
    {
        inputField.text = "";
        numbers[0] = 0;
        numbers[1] = 0;
    }
    public void DeleteLastSymbol()
    {
        string str = inputField.text;
        Debug.Log(str.Length);
        int per = str.Length;
        string newString = "";
        if (per > 0)
        {
            newString = str.Remove(per - 1);
        }        
        inputField.text = newString;        
    }
    public double SuportMethodTryParse(string str)
    {
        string newString = str;
        double num = 0;
        if (double.TryParse(newString, out num))
        {
            num = double.Parse(newString);
        }
        else
        {
            newString = str.Remove(0, 1);
            if (newString.Equals("")) return -1;
            if (str.Contains("%")) newString = str.Remove(str.Length-1);
            num = double.Parse(newString);
        }
        return num;
    }
    public void Percent()
    {
        isPercent = true;
    }
}
